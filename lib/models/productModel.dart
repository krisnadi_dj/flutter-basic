class ProductModel{
  final String id;
  final String code;
  final String productname;
  final String qty;
  final String price;
  final String foto;
  final String createdby;
  final String createdat;

  ProductModel(this.id, this.code, this.productname, this.qty, this.price, this.foto, this.createdby, this.createdat);
}